#ifdef __cplusplus
#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <unordered_map>
using namespace std;
#endif
#ifndef __TANE_H__  //防止被重复包含
#define __TANE_H__  
#ifdef __cplusplus
extern "C" {
#endif
#ifdef __cplusplus

#endif
#ifdef __cplusplus
	void split(const string& s, vector<string>& tokens, const string& delimiters);
#endif
#ifdef __cplusplus
	double compute_e(set<int>& s);
#endif

#ifdef __cplusplus
	void push_back_FD(set<int>& X,int A);
#endif
#ifdef __cplusplus
	void set_print(set<int>& s);
#endif
#ifdef __cplusplus
	vector <vector<string>>* readTable(string file);
#endif
#ifdef __cplusplus
	void clean_data();
#endif
#ifdef __cplusplus
	bool is_superkey(set<int>& s);
#endif
#ifdef __cplusplus
	void find_c_plus_x(set<int> x);
#endif
#ifdef __cplusplus
	void prune();
#endif
#ifdef __cplusplus
	bool is_fd(set<int>& x_except_a, int A);
#endif
#ifdef __cplusplus
	void compute_dependencies();
#endif
#ifdef __cplusplus
	void test();
#endif
#ifdef __cplusplus
	void generate_next_level();
#endif
#ifdef __cplusplus
	int main(int argc, char* argv[]);
#endif

#ifdef __cplusplus
}
#endif
#endif
