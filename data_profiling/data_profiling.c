#include "postgres.h"
#include "fmgr.h"
#include "executor/spi.h"
#include "utils/builtins.h"
#include "string.h"
#include "executor/executor.h"	/* for GetAttributeByName() */
#include "utils/geo_decls.h"	/* for point type */
#include <stdlib.h>   
#include <unistd.h>   
#include <fcntl.h> 			/* for access function */
#include  "TANE.h"
#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

PG_FUNCTION_INFO_V1(export_table);

Datum export_table(PG_FUNCTION_ARGS){
	//传入的参数为导出的tablename
	char*	   tablename = text_to_cstring(PG_GETARG_TEXT_PP(0));
	printf("tablename= %s\n",tablename);
	int32		tablename_size = strlen(tablename);
	printf("tablename_size= %d\n",tablename_size);
	if(tablename_size>64){
		elog(ERROR,"invalid tablename");
		PG_RETURN_BOOL(false);
	}
	// tablename检查---
	// char *command="copy student to '/home/yjl/Desktop/student1.csv' delimiter as',' ENCODING 'utf-8' NULL AS 'null_string';";
	//WITH '
	char* command=(char*)palloc(tablename_size+200);
	char* filepath=(char*)palloc(tablename_size+100);
	//清空char数组
	memset(command,0,tablename_size+200);
	memset(command,0,tablename_size+100);
	strcat(filepath, "/tmp/");
	strcat(filepath, tablename);
	strcat(filepath, ".csv");
	printf("start filepath= %s\n",filepath);
	strcpy(command,"copy ");
	strcat(command,tablename);
	strcat(command, " to '");
	strcat(command,filepath);
	strcat(command,"' delimiter as',' ENCODING 'utf-8' NULL AS 'null_string';");
	printf("command= %s\n",command);
	 if((access(filepath,F_OK))!=-1)   
    {   
        printf("file already exist.\n");   
		printf("___cpp1\n");
    }
	else{
         int ret;
    	uint64 proc;
    	/* 把给定的文本对象转换成一个 C 字符串 */
    	SPI_connect();
    	ret = SPI_exec(command, 0);
    	// proc = SPI_processed;
 		SPI_finish();
	}   
	pfree(command);
	pfree(filepath);
	PG_RETURN_BOOL(true);
	
}

