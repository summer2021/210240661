#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <set>
#include <map>

#include <unordered_map>

#include <functional>

#include <algorithm>

#include "TANE.h"

using namespace std;
set<vector<int>> minfd;
vector <vector<string>>* table;
map <set<int>, int> card;
set <set<int>> L;
map<set<int>, set<int>> c_plus_x;
set<int> R;
void push_back_FD(set<int>& X,int A) {

	//X->A,A放在vector最后面
	vector<int> tmp(X.begin(), X.end());
	tmp.push_back(A);
	minfd.insert(tmp);
}

void display_result(){

	for(auto elem:minfd){

		cout<<"{";

		for(int j=0;j<elem.size()-1;j++){

			if(j==0)cout<<(*table)[0][elem[j]];

			else cout<<','<<(*table)[0][elem[j]];

		}

		cout<<"}->";

		cout<<(*table)[0][elem[elem.size()-1]];

		cout<<"  ";

	}

	cout<<endl;

	

}

void set_print(set<int>& s) {

	for (auto elem : s) {

		cout << elem << ' ';

	}

	cout << endl;

}



vector <vector<string>>* readTable(string file) {

	ifstream ifs;

	ifs.open(file, ios::in);

	string buf;

	int i = 0;

	vector <vector<string>>* table = new vector <vector<string>>();

	while (getline(ifs, buf))

	{

		

		vector<string> tuple;

		split(buf, tuple, ",");

		table->push_back(tuple);

	}

	ifs.close();

	return table;

}

void split(const string& s, vector<string>& tokens, const string& delimiters = ",")

{

	string::size_type lastPos = s.find_first_not_of(delimiters, 0);

	string::size_type pos = s.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {

		tokens.push_back(s.substr(lastPos, pos - lastPos));

		lastPos = s.find_first_not_of(delimiters, pos);

		pos = s.find_first_of(delimiters, lastPos);

	}

}

void clean_data() {

	if (table) {

		delete table;

	}

}

bool is_superkey(set<int>& s) {

	return 	compute_e(s) == 1.0;

}

void find_c_plus_x(set<int> x) {

	set<int> x1(x);//copy of x

	set<int> inter_result=R;



	for (auto elem : x) {

		set<int> tmp_result;

		x1.erase(elem);

		if(c_plus_x.count(x1)==0){

			find_c_plus_x(x1);

		}

		set_intersection(inter_result.begin(), inter_result.end(), c_plus_x[x1].begin(), c_plus_x[x1].end(), inserter(tmp_result,tmp_result.begin()));

		inter_result.swap(tmp_result);

		x1.insert(elem);

	}

	c_plus_x[x] = inter_result;

}

void prune() {

	set<set<int>> delete_set;

	for (auto X : L) {

		if (delete_set.count(X) > 0)continue;

		if (c_plus_x[X].size() == 0) {

			delete_set.insert(X);

		}

		if (is_superkey(X)) {

			//line 5

			for (auto A : c_plus_x[X]) {

				if (X.count(A) == 0) {

					set<int> result=R;

					for (auto B : X) {

						set<int> result2;

						set<int> x_union_a_except_b(X);

						x_union_a_except_b.insert(A);

						x_union_a_except_b.erase(B);

						if (c_plus_x.count(x_union_a_except_b) == 0) {

							cout << "error,c_plus_x don't have the map" << endl;

							find_c_plus_x(x_union_a_except_b);

						}

						set<int>& tmp = c_plus_x[x_union_a_except_b];

						set_intersection(result.begin(), result.end(), tmp.begin(), tmp.end(), inserter(result2, result2.begin()));

						result2.swap(result);

					}

					if (result.count(A) > 0) {

						push_back_FD(X, A);

					}

				}

			}

			//line 8

			delete_set.insert(X);

		}

	}

	for (auto elem : delete_set) {

		L.erase(elem);

	}

}

double compute_e(set<int>& s) {

	unordered_map<string, int> count_card;

	if (card.count(s)>0) {

		return card[s] * 1.0 / table->size();

	}

	else {

		for (int i = 0; i < (table)->size(); i++) {

			string str = "";

			for (auto elem : s) {

				str += (*table)[i][elem];

			}

			count_card[str]++;

		}

		card[s] = count_card.size();

		return (int)count_card.size() * 1.0 / table->size();



	}

	

}

//判断是否函数依赖

bool is_fd(set<int>& x_except_a, int A) {

	set<int>s;

	if (A < 0 || x_except_a.size() == 0)return false;

	double card_x_except_a = compute_e(x_except_a);

	x_except_a.insert(A);

	double card_x = compute_e(x_except_a);

	x_except_a.erase(A);

	if (card_x_except_a == card_x) {

		return true;

	}

	else {

		return false;

	}



}

void compute_dependencies() {



	for (auto X : L) {//set<int> line1

		set<int> X1(X);//copy of X

		set<int> result_set = R;

		for (auto A : X) {

			set<int> result_set2;

			X1.erase(A);

			if (c_plus_x.count(X1) == 0) {

				cout << "error, do not have c_plus_x[X1]" << endl;

				return ;

			}

			set<int> tmp = c_plus_x[X1];

			set_intersection(result_set.begin(), result_set.end(), tmp.begin(), tmp.end(), inserter(result_set2, result_set2.begin()));

			result_set.swap(result_set2);

			X1.insert(A);

		}

		c_plus_x[X] = result_set;

	}

	

	for (auto X : L) {//set<int>,line3

		set<int>& c_plux_xx = c_plus_x[X];//C+(X)

		set<int> x_inter_c;//X&C+(X)

		set_intersection(X.begin(), X.end(), c_plux_xx.begin(), c_plux_xx.end(), inserter(x_inter_c, x_inter_c.begin()));



			for (auto A : x_inter_c){

				set<int> x_except_a(X);

				x_except_a.erase(A);

				if (is_fd(x_except_a, A)) {//line5

					//line6

					push_back_FD(x_except_a, A);

					//line7

					c_plux_xx.erase(A);

					//line8

					for (auto elem : R) {

						if (X.count(elem) == 0) {

							c_plux_xx.erase(elem);

						}

					}



				

			}

		}

	}

}

void test() {

	set<set<int>> s = { {1,2,3},{2,3,4} };

	for (set<set<int>>::iterator i = s.begin(); i != s.end(); i++) {

		set<set<int>>::iterator tmp_it = i;

		tmp_it++;

		for (set<set<int>>::iterator j = tmp_it; j != s.end(); j++) {

			set<int> union_result_set;

			set_union(i->begin(), i->end(), j->begin(), j->end(), inserter(union_result_set, union_result_set.begin()));

			

		}

	}

	system("pause");

}

void generate_next_level() {

	if (L.size() < 2) L.clear();

	set<set<int>> next_L;

	for (set<set<int>>::iterator i = L.begin(); i != L.end();i++) {

		set<set<int>>::iterator tmp_it = i;

		tmp_it++;

		for (set<set<int>>::iterator j = tmp_it; j != L.end(); j++) {

			set<int> union_result_set;

			set_union(i->begin(), i->end(), j->begin(), j->end(),inserter(union_result_set,union_result_set.begin()));

			if (union_result_set.size() == i->size()+1) {

				next_L.insert(union_result_set);

			}

		}

	}

	L.swap(next_L);

	

}

 

int main(int argc, char* argv[])

{

	

	if(argc<2){cout<<"no input csv file"<<endl;return 0;}

	if(argc>2){cout<<"too many input csv files,please enter one file"<<endl;return 0;}

// int fd_finding(){

	char* tablename=argv[1];

	cout<<tablename<<endl;

	long start=clock();

	string filepath = "/tmp/";

	filepath+=tablename;

	filepath+=".csv";

	table = readTable(filepath);

	int row_num = table->size();//tuple_num

	if (row_num == 0) { cout << "no tuple" << endl; return 0; }

	int col_num = (*table)[0].size();//attr_num

	

	for (int i = 0; i < col_num; i++) {

		R.insert(i);

	}

	//line2

	c_plus_x[set<int>()] = R;

	//line3

	

	for (auto elem : R) {

		set<int>s;

		s.insert(elem);

		L.insert(s);

	}

	

	//line5 computer_dependency

	while (L.size()) {

		// function computer_dependency

		compute_dependencies();

		prune();

		generate_next_level();

	}

	long end = clock();

	

	cout << "spend" << (end - start) / CLOCKS_PER_SEC<<"s\n";

	cout <<"There are "<< minfd.size()<<" min_FDs:" << endl;

	display_result();

	clean_data();



}










